<?php

return [
    'twig' => [
        'templates_dirs' => [

        ],
        'options' => [
            'debug' => false,
            'charset' => 'UTF-8',
            'cache' => false, //'/var/www/auth-apc/var/cache'
            'auto_reload' => null,
            'strict_variables' => false,
            'autoescape' => 'html',
            'optimizations' => -1,
        ],
    ]
];
