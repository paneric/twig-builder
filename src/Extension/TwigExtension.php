<?php
/**
 * Slim Framework (http://slimframework.com)
 *
 * @license   https://github.com/slimphp/Twig-View/blob/master/LICENSE.md (MIT License)
 */

declare(strict_types=1);

namespace Paneric\Twig\Extension;

use Psr\Http\Message\UriInterface;
use Slim\Interfaces\RouteParserInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
    public function __construct(
        protected RouteParserInterface $routeParserInterface,
        protected UriInterface $uri,
        protected string $routeName,
        protected string $basePath = '',
        protected string $baseUrl = ''
    ) {
    }

    public function getName(): string
    {
        return 'twig';
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('route_name', [$this, 'getRouteName']),
            new TwigFunction('uri_for', [$this, 'getUriFor']),
            new TwigFunction('url_for', [$this, 'getUrlFor']),
            new TwigFunction('full_uri', [$this, 'getFullUri']),
            new TwigFunction('full_url', [$this, 'getFullUrl']),//BLAD

            new TwigFunction('is_current_url', [$this, 'isCurrentUrl']),

            new TwigFunction('base_path', [$this, 'getBasePath']),
            new TwigFunction('base_url', [$this, 'getBaseUrl']),
        ];
    }

    public function getRouteName(): string
    {
        return $this->routeName;
    }

    public function getUriFor(string $routeName, array $data = [], $queryParams = []): string
    {
        return $this->routeParserInterface->urlFor($routeName, $data, $queryParams);
    }

    public function getUrlFor(string $routeName, array $data = [], array $queryParams = []): string
    {
        return $this->routeParserInterface->fullUrlFor($this->uri, $routeName, $data, $queryParams);
    }

    public function isCurrentUrl(string $routeName, $data = []): bool
    {
        $currentUrl = $this->baseUrl . $this->uri->getPath();
        $result = $this->routeParserInterface->urlFor($routeName, $data) ;

        return $result === $currentUrl;
    }

    public function getFullUrl(bool $withQueryString = false): string
    {
        $currentUrl = $this->baseUrl . $this->uri->getPath();

        $query = $this->uri->getQuery();

        if ($withQueryString && !empty($query)) {
            $currentUrl .= '?' . $query;
        }

        return $currentUrl;
    }

    public function getFullUri(): string
    {
        return $this->uri->getPath();
    }

    public function setUri(UriInterface $uri): self
    {
        $this->uri = $uri;
        return $this;
    }

    public function getBasePath(): string
    {
        return $this->basePath;
    }

    public function setBasePath(string $basePath): self
    {
        $this->basePath = $basePath;
        return $this;
    }

    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }
}
