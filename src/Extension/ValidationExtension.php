<?php

declare(strict_types=1);

namespace Paneric\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ValidationExtension extends AbstractExtension
{
    protected array $messages;

    public function getName(): string
    {
        return 'validation';
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('vld_set_messages', [$this, 'vldSetMessages']),
            new TwigFunction('vld_style', [$this, 'vldStyle']),
            new TwigFunction('vld_dti', [$this, 'vldDTI']),
            new TwigFunction('vld_feedback', [$this, 'vldFeedback']),
        ];
    }

    public function vldSetMessages(array $report = null): void
    {
        $this->messages = [];

        if ($report !== null) {
            foreach ($report as $messages){
                foreach($messages as $key => $msg) {
                    $this->messages[$key] = $msg;
                }
            }
        }
    }

    public function vldStyle(string $field, $key = null): string
    {
        if (array_key_exists($field, $this->messages)) {

            if ($key === null) {
                return 'is-invalid';
            }

            if (
                is_array($this->messages[$field]) &&
                array_key_exists($key, $this->messages[$field])
            ) {
                return 'is-invalid';
            }
        }

        return '';
    }

    public function vldFeedback(string $field, $key = null): string
    {
        $fieldAlerts = [];

        if (array_key_exists($field, $this->messages)) {

            if (is_array($this->messages[$field]) && $key !== null) {
                $fieldAlerts = $this->messages[$field][$key];
            }

            if ($key === null) {
                $fieldAlerts = $this->messages[$field];
            }

            if (empty($fieldAlerts)) {
                return '';
            }

            $alerts = '<div class="validation-feedback"><br>';

            foreach ($fieldAlerts as $alert) {
                $alerts .= $alert . '<br>';
            }

            $alerts .= '</div>';

            return $alerts;
        }

        return '';
    }
}
