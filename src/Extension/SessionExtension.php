<?php

declare(strict_types=1);

namespace Paneric\Twig\Extension;

use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Interfaces\Translator\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SessionExtension extends AbstractExtension
{
    private array $alertTypes = [
        'error' => 'alert-danger',
        'warning' => 'alert-warning',
        'info' => 'alert-info',
        'success' => 'alert-success'
    ];

    public function __construct(protected SessionInterface $session, protected TranslatorInterface $translator)
    {
    }

    public function getName(): string
    {
        return 'session';
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('session', [$this, 'session']),
            new TwigFunction('flash', [$this, 'flash']),
            new TwigFunction('mode', [$this, 'mode']),
            new TwigFunction('local', [$this, 'local']),
        ];
    }

    public function session(string $key): string|int|array
    {
        return $this->session->getData($key);
    }

    public function flash(string $key): string|array|null
    {
        $flashData = $this->session->getFlash($key);

        if ($key === 'value') {
            return $flashData;
        }

        $flashMessages = '';

        if (!empty($flashData)) {
            foreach ($flashData as $message) {
                $flashMessages .= '<p>' . $this->translator->trans($message) . '</p>';
            }
        }

        if ($flashMessages === '') {
            return $flashMessages;
        }

        return sprintf(
            '<div class="alert %s" role="alert">%s</div>'
            , $this->alertTypes[$key],
            $flashMessages
        );
    }

    public function mode(): ?string
    {
        $authentication = $this->session->getData('account');
        $apiToken = $this->session->getData('api_token');

        if (empty($authentication) || empty($apiToken)) {
            return 'visitor';
        }

        return 'user';
    }

    public function local(): string
    {
        return $this->session->getData('local');
    }
}
