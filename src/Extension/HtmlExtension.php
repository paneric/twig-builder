<?php

declare(strict_types=1);

namespace Paneric\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class HtmlExtension extends AbstractExtension
{
    public function getName(): string
    {
        return 'html';
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('active', [$this, 'active']),
        ];
    }

    public function active(string $menuItem, string $active): string
    {
        return $menuItem === $active ? 'active' : '';
    }
}
