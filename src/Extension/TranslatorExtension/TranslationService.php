<?php

declare(strict_types=1);

namespace Paneric\Twig\Extension\TranslatorExtension;

use InvalidArgumentException;
use Paneric\Interfaces\Translator\TranslatorInterface;

class TranslationService implements TranslatorInterface
{
    public function __construct(protected string $locale, protected array $translations)
    {
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function trans(string $id, array $parameters = [], string $domain = null, string $locale = null): string
    {
        if (empty($this->translations[$id])) {
            throw new InvalidArgumentException;
        }
        return $this->translations[$id];
    }

    /**
     * @throws InvalidArgumentException
     */
    public function dict(string $routePath): array
    {
        if (empty($this->translations[$routePath]) || !is_array($this->translations[$routePath])) {
            throw new InvalidArgumentException;
        }
        return $this->translations[$routePath];
    }
}
