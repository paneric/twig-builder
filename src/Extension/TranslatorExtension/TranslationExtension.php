<?php

declare(strict_types=1);

namespace Paneric\Twig\Extension\TranslatorExtension;

use InvalidArgumentException;
use Paneric\Interfaces\Translator\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TranslationExtension extends AbstractExtension
{
    public function __construct(protected TranslatorInterface $translationService)
    {
    }

    public function getName(): string
    {
        return 'translation';
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('locale', [$this, 'getLocale']),
            new TwigFunction('get_dictionary', [$this, 'getDictionary']),
            new TwigFunction('trans', [$this, 'trans']),

        ];
    }

    public function getLocale(): string
    {
        return $this->translationService->getLocale();
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getDictionary(string $routeName): array
    {
        return $this->translationService->dict($routeName);
    }

    /**
     * @throws InvalidArgumentException
     */
    public function trans(string $id): string
    {
        return $this->translationService->trans($id);
    }
}
