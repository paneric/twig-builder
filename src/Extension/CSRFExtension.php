<?php

declare(strict_types=1);

namespace Paneric\Twig\Extension;

use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CSRFExtension extends AbstractExtension
{
    public function __construct(
        protected SessionInterface $session,
        protected GuardInterface $guard,
        protected array $config
    ) {
    }

    public function getName(): string
    {
        return 'csrf';
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('csrf', [$this, 'csrf']),
        ];
    }

    public function csrf(): string
    {
        $csrfKey = $this->guard->generateRandomString($this->config['csrf_key_length']);
        $this->session->setData($csrfKey, $this->config['csrf_key_name']);

        return sprintf(
            '<input type="hidden" name="%s" value="%s">',
            $this->config['csrf_hash_name'],
            $this->guard->hash($csrfKey)
        );
    }
}
