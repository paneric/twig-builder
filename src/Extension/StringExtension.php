<?php

declare(strict_types=1);

namespace Paneric\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class StringExtension extends AbstractExtension
{
    public function getName(): string
    {
        return 'string';
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('plural', [$this, 'plural']),
            new TwigFunction('replace', [$this, 'replace']),
        ];
    }

    public function plural(string $string): string
    {
        return substr($string, -1) === 's' ? $string . 'es' : $string . 's';
    }

    public function replace(string $search, string $replace, string $subject): string
    {
        return str_replace($search, $replace, $subject);
    }
}
