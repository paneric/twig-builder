<?php

declare(strict_types=1);

namespace Paneric\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ArrayExtension extends AbstractExtension
{
    public function getName(): string
    {
        return 'array';
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('has_key', [$this, 'hasKey']),
            new TwigFunction('has_value', [$this, 'hasValue']),
            new TwigFunction('cascade', [$this, 'cascade']),
            new TwigFunction('has_same_length', [$this, 'hasSameLength']),
            new TwigFunction('count', [$this, 'count']),
        ];
    }

    public function hasKey(array $array, $key): bool
    {
        return empty($array) ? false : array_key_exists($key, $array);
    }

    public function hasValue(array $array, $value, bool $strict = false): bool
    {
        return empty($array) ? false : in_array($value, $array, $strict);
    }

    public function cascade(array $array, string $ref, string $local): array
    {
        $refElements = explode('_', $ref);

        $result = [];

        $currentRef = '';

        foreach ($array as $key => $row) {

            if ($currentRef !== $row[$ref]) {
                $currentRef = $row[$ref];

                $result[$currentRef]['label'] = $row[$refElements[0] . '_' . $local];
                $result[$currentRef]['id'] = $row[$refElements[0] . '_id'];
            }

            $result[$currentRef]['items'][] = $row;
        }

        return $result;
    }

    public function hasSameLength(array $a, array $b): bool
    {
        return count($a) === count($b);
    }

    public function count(array $a): int
    {
        return count($a);
    }
}
