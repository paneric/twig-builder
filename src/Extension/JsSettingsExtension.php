<?php

declare(strict_types=1);

namespace Paneric\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class JsSettingsExtension extends AbstractExtension
{
    public function __construct(protected ?array $jsSettings = null)
    {
    }

    public function getName(): string
    {
        return 'js-settings';
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_js_settings', [$this, 'getJsSettings']),
        ];
    }

    public function getJsSettings(string $routeName): ?array
    {
        if (empty($this->jsSettings) || empty($this->jsSettings[$routeName])) {
            return null;
        }
        return $this->jsSettings[$routeName];
    }
}
