<?php

declare(strict_types=1);

namespace Paneric\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class DateTimeExtension extends AbstractExtension
{
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function getName(): string
    {
        return 'date_time';
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('add_interval', [$this, 'addInterval']),
        ];
    }

    public function addInterval(string $format, string $unit, int $value, string $dateTimeString = null): string
    {
        return '';
    }
}
