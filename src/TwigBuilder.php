<?php

declare(strict_types=1);

namespace Paneric\Twig;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Loader\FilesystemLoader;

class TwigBuilder
{
    public function build(array $settings, string $basePath = ''): Environment
    {
        return new Environment(
            $this->setLoader($settings['templates_dirs'], $basePath),
            $settings['options']
        );
    }

    private function setLoader(array $templatesDirs, string $basePath = ''): ?FilesystemLoader
    {
        try {
            $loader = new FilesystemLoader([], $basePath);

            foreach ($templatesDirs as $dir => $namespace) {
                if (is_numeric($dir) || !is_string($dir)) {
                    $loader->addPath($namespace);
                }

                if (!is_numeric($dir) || is_string($dir)) {
                    $loader->addPath($dir, $namespace);
                }
            }

            return $loader;
        } catch (LoaderError $e) {
            echo $e->getMessage();
        }

        return null;
    }
}
